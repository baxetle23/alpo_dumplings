#! /bin/bash

set -xe

echo "BACKEND_VERSION=${BACKEND_VERSION}" > /home/backend/backend_env.conf
echo "FRONTEND_VERSION=${FRONTEND_VERSION}" >> /home/backend/backend_env.conf


sudo docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}
# sudo docker rm -f momo-backend || true
sudo docker-compose -f /home/backend/docker-compose.yml --env-file  /home/backend/backend_env.conf up -d --force-recreate momo-backend 

# sudo docker-compose -f /home/backend/docker-compose.yml --env-file  /home/backend/backend_env.conf up -d --force-recreate backend 
    