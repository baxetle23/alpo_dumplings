# module "yandex_cloud_network" {
#     source = "./modules/tf_yc_network"
# }

# module "yandex_cloud_instance" {
#     source = "./modules/tf_yc_instance"
# }


module "kube" {
  source     = "./modules/tf_yc_kubernetes"
  network_id = "enppblfqhc5eujgm6lfl"

  master_locations   = [
    {
      zone      = "ru-central1-a"
      subnet_id = "e9bk0nofq0tlmj7c3jdo"
    },
    {
      zone      = "ru-central1-b"
      subnet_id = "e2lsalklj8of5444djlc"
    },
    {
      zone      = "ru-central1-d"
      subnet_id = "fl801ql4j5shjr67la9p"
    }
  ]

  master_maintenance_windows = [
    {
      day        = "monday"
      start_time = "23:00"
      duration   = "3h"
    }
  ]

  node_groups = {
    "yc-k8s-ng-01" = {
      description  = "Kubernetes nodes group 01"
      fixed_scale   = {
        size = 3
      }
      node_labels   = {
        role        = "worker-01"
        environment = "testing"
      }
    },
    "yc-k8s-ng-02"  = {
      description   = "Kubernetes nodes group 02"
      auto_scale    = {
        min         = 2
        max         = 4
        initial     = 2
      }
      node_locations   = [
        {
          zone      = "ru-central1-b"
          subnet_id = "e2lsalklj8of5444djlc"
        }
      ]
      node_labels   = {
        role        = "worker-02"
        environment = "dev"
      }
      max_expansion   = 1
      max_unavailable = 1
    }
  }
}