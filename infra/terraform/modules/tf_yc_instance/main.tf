module "yandex_cloud_network" {
    source = "../tf_yc_network"
}

resource "yandex_compute_instance" "vm-1" {
    name = var.name_vm
    
    zone =  module.yandex_cloud_network.network_zone
    # hostname = "std-024-15_v2.praktikum-services"

    # Конфигурация ресурсов:
    # количество процессоров и оперативной памяти
    resources {
        cores  = var.count_core
        memory = var.count_memory
    }

    # Загрузочный диск:
    # здесь указывается образ операционной системы
    # для новой виртуальной машины
    boot_disk {
        initialize_params {
            image_id = var.image_id
        }
    }

    # Сетевой интерфейс:
    # нужно указать идентификатор подсети, к которой будет подключена ВМ
    network_interface {
        subnet_id = module.yandex_cloud_network.yandex_vpc_subnets.subnet_id
        nat       = true
    }

    # Метаданные машины:
    # здесь можно указать скрипт, который запустится при создании ВМ
    # или список SSH-ключей для доступа на ВМ
    metadata = {
        //ssh-keys  = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
        user-data = "${file("./files/cloud_config")}"
    }
}