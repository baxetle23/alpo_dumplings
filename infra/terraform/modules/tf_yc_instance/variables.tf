
variable "image_id" {
  default = "fd80qm01ah03dkqb14lc"
  description = "Required"
}

variable "name_vm" {
  default = "lcash-dumplings"
  description = "Optional"
}

variable "count_core" {
  default = 2
  description = "Required"
}

variable "count_memory" {
  default = 2
  description = "Required"
}