variable "network_zone" {
  description = "Yandex.Cloud network availability zones"
  type        = string
  default     = "ru-central1-a"
}


provider "yandex" {
  cloud_id  = var.cloud_id
  folder_id = var.folder_id
  # zone      = module.yandex_cloud_network.network_zone
}