## Московская пельменная -> https://moscow-dumplings.ru

<img width="900" alt="image" src="https://user-images.githubusercontent.com/9394918/167876466-2c530828-d658-4efe-9064-825626cc6db5.png">

## Проект

### Frontend
для сборки локально:
```bash
npm install
NODE_ENV=production VUE_APP_API_URL=http://localhost:8081 npm run serve
```

### Backend
для сборки локально:
```bash
go run ./cmd/api
go test -v ./... 
```

### Continious Integration

Описано в директории frontend
> frontend: sonar анализ -> сборка образа -> хранение в gitlab registry -> в зависимости от имени ветки формирование версии 

Описано в директории backend
> backend: build проектв -> sonar анализ -> sast анализ -> сборка образа -> хранение в gitlab registry -> в зависимости от имени ветки формирование версии

### Continious Deployment

Описано в директории infra
> terraform: выделение ресурсов в yandex cloud с помощью Terraform, с хранением состояния в s3

> helm: деплоймент происходит с помощью helm и helm манифестов по кнопке в джобе, версионирование и хранение в nexus:
  https://nexus.praktikum-services.tech/service/rest/repository/browse/std-024-15-helm-repo/
  
> установка сертификатов и loadBalancera в ручную

> Так же можно использовать docker-compose - процесс не автоматизирвоан

## Доменное имя
На текущий момент(пока есть деньги на промокоде в yandex cloud) куплен домен moscow-dumplings.ru на reg.ru. Указаны адреса серверов имен Yandex Cloud в DNS-записях  регистратора:

- ns1.yandexcloud.net
- ns2.yandexcloud.net