#! /bin/bash

set -xe

echo "FRONTEND_VERSION=${FRONTEND_VERSION}" > /home/frontend/frontend_env.conf
echo "BACKEND_VERSION=${BACKEND_VERSION}" >> /home/frontend/frontend_env.conf


sudo docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}
# sudo docker rm -f momo-frontend || true
sudo docker-compose -f /home/frontend/docker-compose.yml --env-file  /home/frontend/frontend_env.conf up -d --force-recreate momo-frontend 

# sudo docker-compose -f /home/backend/docker-compose.yml --env-file  /home/backend/backend_env.conf up -d --force-recreate backend 
    